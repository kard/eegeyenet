# configuration used by the training and evaluation methods
# let's keep it here to have a clean code on other methods that we try
import time
import os
from Clusters.cluster import clustering as clustering
from Clusters.cluster2 import clustering as clustering2
from Clusters.cluster3 import clustering as clustering3
config = dict()

##################################################################
# Please note that the following fields will be set by our scripts to re-train and re-evaluate our model.
# Where experiment results are stored.
config['log_dir'] = './runs/'
# Path to training, validation and test data folders.
# PS: Note that we have to upload the data to the server!!!
config['measured_data_dir'] = './data/measured/'
config['prepared_data_dir'] = './data/prepared/'
# Path of root
config['root_dir'] = '.'

##################################################################
# You can modify the rest or add new fields as you need.
# Hyper-parameters and training configuration.
config['learning_rate'] = 1e-4

"""
Parameters that can be chosen:

cnn: The simple CNN architecture
inception: The InceptionTime architecture
eegnet: The EEGNet architecture
deepeye: The Deep Eye architecture
xception: The Xception architecture
deepeye-rnn: The DeepEye RNN architecture
simple_classifier: Tries the simple classifiers from sklearn.

If downsampled set to true, the downsampled data (which have length 125) will be used. Otherwise, full data with length 500 is used.
If split set to true, the data will be clustered and fed each to a separate architecture. The extracted features are concatenated
finally used for classification.
Cluster can be set to clustering(), clustering2() or clustering3(), where different clusters based on literature are used.
"""

# Choosing model
config['model'] = 'simple_classifier'
config['downsampled'] = False
config['split'] = False
config['cluster'] = clustering()


config['ensemble'] = 5 #number of models in the ensemble method
config['trainX_file'] = 'noweEEG.mat' if config['downsampled'] else 'all_EEGprocuesan.mat'
config['trainY_file'] = 'all_trialinfoprosan.mat'
config['trainX_variable'] = 'noweEEG' if config['downsampled'] else 'all_EEGprocuesan'
config['trainY_variable'] = 'all_trialinfoprosan'

# CNN
config['cnn'] = {}
# CNN
config['pyramidal_cnn'] = {}
# InceptionTime
config['inception'] = {}
# DeepEye
config['deepeye'] = {}
# Xception
config['xception'] = {}
# EEGNet
config['eegnet'] = {}
# DeepEye-RNN
config['deepeye-rnn'] = {}

config['cnn']['input_shape'] = (125, 129) if config['downsampled'] else (500, 129)
config['pyramidal_cnn']['input_shape'] = (125, 129) if config['downsampled'] else (500, 129)
config['inception']['input_shape'] = (125, 129) if config['downsampled'] else (500, 129)
config['deepeye']['input_shape'] = (125, 129) if config['downsampled'] else (500, 129)
config['deepeye-rnn']['input_shape'] = (125, 129) if config['downsampled'] else (500, 129)
config['xception']['input_shape'] = (125, 129) if config['downsampled'] else (500, 129)
config['eegnet']['channels'] = 129
config['eegnet']['samples'] = 125 if config['downsampled'] else 500

# Create a unique output directory for this experiment.
timestamp = str(int(time.time()))
model_folder_name = timestamp if config['model'] == '' else timestamp + "_" + config['model']

if config['split'] and config['model'] != 'simple_classifier':
     model_folder_name += '_cluster'
if config['downsampled'] and config['model'] != 'simple_classifier':
    model_folder_name += '_downsampled'
if config['ensemble'] > 1 and config['model'] != 'simple_classifier':
    model_folder_name += '_ensemble'

config['model_dir'] = os.path.abspath(os.path.join(config['log_dir'], model_folder_name))
if not os.path.exists(config['model_dir']):
    os.makedirs(config['model_dir'])

config['info_log'] = config['model_dir'] + '/' + 'info.log'
config['batches_log'] = config['model_dir'] + '/' + 'batches.log'
