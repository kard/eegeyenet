import numpy as np
def clustering():
    c1=list(np.array([15,18,16,10,11,12,5,6,7,106,31,80])-1)#mfront
    c2=list(np.array([55,54,79,78,77,76,75,71,67,61,72,62])-1) #mparietal
    c3=list(np.array([45,46,41,42,37,51,52,70,53,47,60,66,76,65,58,59])-1) #lparietal
    c4=list(np.array([87,86,93,85,92,98,103,102,97,91,84,83,90,96,101,108])-1)#rparietal
    c5=list(np.array([33,26,22,34,27,23,35,28,24,19,36,29,20,30,13])-1) #lfront
    c6=list(np.array([9,2,122,3,123,116,4,124,117,110,118,111,104,112,105])-1) #rfront
    
    cluster3={}

    cluster3['c1']=c1
    cluster3['c2']=c2
    cluster3['c3']=c3
    cluster3['c4']=c4
    cluster3['c5']=c5
    cluster3['c6']=c6
    
    return cluster3
