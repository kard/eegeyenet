import numpy as np
def clustering():
    c1=list(np.array([12,5,4,11,19,18,16,10,15,9,22])-1)#front
    c2=list(np.array([8,1,2,125,126,127,14,21,25,26,31,128])-1) #EOG
    c3=list(np.array([114,115,109,108,113,107,103,110,111,104,93,102,96,91,102,98,92,116])-1) #right
    c4=list(np.array([56,49,44,39,45,50,58,59,51,46,40,34,35,41,47,52,42,36])-1)#left
    c5=list(np.array([37,30,29,6,112,7,106,105,111,31,80,87,31,55,79,78,61,54])-1) #cent
    c6=list(np.array([67,77,72,66,71,76,70,75,83,69,74,82,89,68,94,73,88,69])-1) #occi
    
    cluster2={}

    cluster2['c1']=c1
    cluster2['c2']=c2
    cluster2['c3']=c3
    cluster2['c4']=c4
    cluster2['c5']=c5
    cluster2['c6']=c6
    
    return cluster2
