import numpy as np
def clustering():
    c1=list(np.array([12,5,4,11,19,18,16,10,15,9,14,17,21,22,126,127])-1)
    c2=list(np.array([8,1,2,122,121,116,123,3,124,117,118])-1)
    c3=list(np.array([114,115,109,108,102,98,103,110,111,104,93])-1)
    c4=list(np.array([100,101,97,96,95,89,90,84,91,85,92,86])-1)
    c5=list(np.array([82,74,70,75,83,76,71,67,72,77,78,62,61])-1)
    c6=list(np.array([69,65,64,58,57,50,51,59,66,60,52,53])-1)
    c7=list(np.array([44,39,40,45,46,47,41,35,29,36,42])-1)
    c8=list(np.array([38,33,32,25,26,23,27,34,28,24,20])-1)
    c9=list(np.array([13,6,112,105,106,7,30,37,31,129,80,87,79,55,54])-1)
    cluster={}
    cluster['c1']=c1
    cluster['c2']=c2
    cluster['c3']=c3
    cluster['c4']=c4
    cluster['c5']=c5
    cluster['c6']=c6
    cluster['c7']=c7
    cluster['c8']=c8
    cluster['c9']=c9
    return cluster
