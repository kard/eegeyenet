import tensorflow as tf
from config import config
from utils.utils import *
import logging
from ConvNet import ConvNet


class Classifier_XCEPTION(ConvNet):
    """
    The Xception architecture. This is inspired by Xception paper, which describes how 'extreme' convolutions can be represented
    as separable convolutions and can achieve better accuracy then the Inception architecture. It is made of modules in a specific depth.
    Each module, in our implementation, consists of a separable convolution followed by batch normalization and a ReLu activation layer.
    """
    def __init__(self, input_shape, kernel_size=40, nb_filters=128, verbose=True, epochs=1, batch_size=64, use_residual=True, depth=6):
        super(Classifier_XCEPTION, self).__init__(input_shape, kernel_size=kernel_size, nb_filters=nb_filters,
                                                  verbose=verbose, epochs=epochs, batch_size=batch_size, use_residual=use_residual, depth=depth,
                                                  preprocessing=False)

    def _module(self, input_tensor, current_depth):
        """
        The module of Xception. Consists of a separable convolution followed by batch normalization and a ReLu activation function.
        """
        x = tf.keras.layers.SeparableConv1D(filters=self.nb_filters, kernel_size=self.kernel_size, padding='same', use_bias=False, depth_multiplier=1)(input_tensor)
        x = tf.keras.layers.BatchNormalization()(x)
        x = tf.keras.layers.Activation(activation='relu')(x)
        return x
