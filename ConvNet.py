from abc import ABC, abstractmethod

from sklearn.model_selection import train_test_split
import tensorflow as tf
import tensorflow.keras as keras
from config import config
from keras.callbacks import CSVLogger
import logging

class prediction_history(tf.keras.callbacks.Callback):
    def __init__(self,validation_data):
        self.validation_data = validation_data
        self.predhis = []
        self.targets = validation_data[1]


    def on_epoch_end(self, epoch, logs={}):
        y_pred = self.model.predict(self.validation_data[0])
        self.predhis.append(y_pred)


class ConvNet(ABC):
    def __init__(self, input_shape, kernel_size=32, nb_filters=32, verbose=True, batch_size=64, use_residual=False, depth=6, epochs=2, preprocessing = False):
        self.use_residual = use_residual
        self.depth = depth
        self.callbacks = None
        self.batch_size = batch_size
        self.verbose = verbose
        self.kernel_size = kernel_size
        self.nb_filters = nb_filters
        self.preprocessing = preprocessing
        self.input_shape = input_shape
        self.epochs = epochs
        logging.info('Parameters: ')
        logging.info('--------------- use residual : ' + str(self.use_residual))
        logging.info('--------------- depth        : ' + str(self.depth))
        logging.info('--------------- batch size   : ' + str(self.batch_size))
        logging.info('--------------- kernel size  : ' + str(self.kernel_size))
        logging.info('--------------- nb filters   : ' + str(self.nb_filters))
        logging.info('--------------- preprocessing: ' + str(self.preprocessing))

        if config['split']:
            self.model = self._split_model()
        else:
            self.model = self._build_model()
        
        self.model.compile(loss='binary_crossentropy', optimizer=keras.optimizers.Adam(), metrics=['accuracy'])
        if self.verbose:
            self.model.summary()

    def _split_model(self):
        input_layer = keras.layers.Input(self.input_shape)
        output = []

        for c in config['cluster'].keys():
            a = [self.input_shape[0]]
            a.append(len(config['cluster'][c]))
            input_shape = tuple(a)
            output.append(self._build_model(X=tf.transpose(tf.nn.embedding_lookup(tf.transpose(input_layer), config['cluster'][c]))))

        # append the results and perform 1 dense layer with last_channel dimension and the output layer
        x = tf.keras.layers.Concatenate()(output)
        dense = tf.keras.layers.Dense(32, activation='relu')(x)
        output_layer = tf.keras.layers.Dense(1, activation='sigmoid')(dense)
        model = tf.keras.models.Model(inputs=input_layer, outputs=output_layer)
        return model

    # abstract method
    def _preprocessing(self, input_tensor):
        pass

    # abstract method
    def _module(self, input_tensor, current_depth):
        pass

    def _shortcut_layer(self, input_tensor, out_tensor):
        shortcut_y = tf.keras.layers.Conv1D(filters=int(out_tensor.shape[-1]), kernel_size=1, padding='same', use_bias=False)(input_tensor)
        shortcut_y = tf.keras.layers.BatchNormalization()(shortcut_y)
        x = keras.layers.Add()([shortcut_y, out_tensor])
        x = keras.layers.Activation('relu')(x)
        return x

    def _build_model(self, X=[]):
        if config['split']:
            input_layer = X
        else:
            input_layer = tf.keras.layers.Input(self.input_shape)

        if self.preprocessing:
            preprocessed = self._preprocessing(input_layer)
            x = preprocessed
            input_res = preprocessed
        else:
            x = input_layer
            input_res = input_layer

        for d in range(self.depth):
            x = self._module(x, d)
            if self.use_residual and d % 3 == 2:
                x = self._shortcut_layer(input_res, x)
                input_res = x

        gap_layer = tf.keras.layers.GlobalAveragePooling1D()(x)
        if config['split']:
            return gap_layer
        output_layer = tf.keras.layers.Dense(1, activation='sigmoid')(gap_layer)
        model = tf.keras.models.Model(inputs=input_layer, outputs=output_layer)
        return model

    def get_model(self):
        return self.model

    def fit(self, x, y):
        csv_logger = CSVLogger(config['batches_log'], append=True, separator=';')
        # early_stop = tf.keras.callbacks.EarlyStopping(monitor='val_accuracy', patience=20)
        ckpt_dir = config['model_dir'] + '/' + config['model'] + '_' + 'best_model.h5'
        ckpt = tf.keras.callbacks.ModelCheckpoint(ckpt_dir, verbose=1, monitor='val_accuracy', save_best_only=True, mode='auto')
        X_train, X_val, y_train, y_val = train_test_split(x, y, test_size=0.199182, shuffle=False)
        prediction_ensemble = prediction_history((X_val,y_val))
        hist = self.model.fit(X_train, y_train, verbose=2, batch_size=self.batch_size, validation_data=(X_val,y_val),
                              epochs=self.epochs, callbacks=[csv_logger, ckpt, prediction_ensemble])
        return hist, prediction_ensemble
