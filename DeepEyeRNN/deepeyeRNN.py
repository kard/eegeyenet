import tensorflow as tf
from config import config
from utils.utils import *
import logging
from ConvNet import ConvNet
from tensorflow.keras.constraints import max_norm


class Classifier_DEEPEYE_RNN(ConvNet):
    """
    The Classifier_DeepEye_RNN is the architecture that combines the DeepEye architecture with an RNN (this is done, based
    on our feedback of preproposal). It uses 2 LSTMs as a preprocessing step (instead of EEGNet inspired preprocessing) to
    extract features which are feeded later to our main architecture of DeepEye.
    """

    def __init__(self, input_shape, kernel_size=40, nb_filters=32, verbose=True, batch_size=64, use_residual=True,
                 depth=6, bottleneck_size=32, preprocessing=True, preprocessing_feature_nb = 8, dropoutRate = 0.25):
        """
        The DeepEye architecture has the following basic structures. It offers the possibility to do a preprocessing with RNNs.
        It is made of modules of specific depth. Each module is made the inceptionTime submodule, a separable convolution and a simple
        convolution with max pooling for stability reasons.
        """

        self.preprocessing_feature_nb = preprocessing_feature_nb
        self.dropoutRate = dropoutRate
        self.bottleneck_size = bottleneck_size
        super(Classifier_DEEPEYE_RNN, self).__init__(input_shape=input_shape, kernel_size=kernel_size, nb_filters=nb_filters,
                                                 verbose=verbose, batch_size=batch_size, use_residual=use_residual,
                                                 depth=depth, preprocessing=preprocessing)

    def _preprocessing(self, input_tensor):
        lstm = tf.keras.Sequential()
        lstm.add(tf.keras.layers.LSTM(self.preprocessing_feature_nb, return_sequences=True))
        lstm.add(tf.keras.layers.Dropout(self.dropoutRate))
        lstm.add(tf.keras.layers.LSTM(self.preprocessing_feature_nb, return_sequences=True))
        lstm.add(tf.keras.layers.Dropout(self.dropoutRate))
        lstm.add(tf.keras.layers.BatchNormalization())
        print(input_tensor.shape)
        output_tensor = lstm(input_tensor)
        return output_tensor

    def _module(self, input_tensor, current_depth):
        if int(input_tensor.shape[-1]) > 1:
            input_inception = tf.keras.layers.Conv1D(filters=self.bottleneck_size, kernel_size=1, padding='same', use_bias=False)(input_tensor)
        else:
            input_inception = input_tensor

        kernel_size_s = [self.kernel_size // (2 ** i) for i in range(3)]
        conv_list = []

        for i in range(len(kernel_size_s)):
            conv_list.append(
                tf.keras.layers.Conv1D(filters=self.nb_filters, kernel_size=kernel_size_s[i], padding='same', use_bias=False)(input_inception))

        max_pool_1 = tf.keras.layers.MaxPool1D(pool_size=10, strides=1, padding='same')(input_tensor)
        conv_6 = tf.keras.layers.Conv1D(filters=self.nb_filters, kernel_size=1, padding='same', use_bias=False)(max_pool_1)
        conv_list.append(conv_6)

        x = tf.keras.layers.Concatenate(axis=2)(conv_list)
        x = tf.keras.layers.BatchNormalization()(x)
        x = tf.keras.layers.Activation(activation='relu')(x)
        return x
