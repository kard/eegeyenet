from config import config
import numpy as np
import scipy.io as sio
import os
import pickle
import h5py
import logging

def get_mat_data(data_dir, verbose=True):
    with h5py.File(data_dir + config['trainX_file'], 'r') as f:
        X = f[config['trainX_variable']][:]
        if verbose:
            logging.info("X training loaded.")
            logging.info(X.shape)

    with h5py.File(data_dir + config['trainY_file'], 'r') as f:
        y = f[config['trainY_variable']][:]
        if verbose:
            logging.info("y training loaded.")
            logging.info(y.shape)

    if verbose: logging.info("Setting the shapes")
    X = np.transpose(X, (2, 1, 0))
    y = np.transpose(y, (1, 0))
    if config['downsampled']: X = np.transpose(X, (0, 2, 1))
    if config['model'] == 'eegnet' or config['model'] == 'eegnet_cluster':
        X = np.transpose(X, (0, 2, 1))
    if verbose:
        logging.info(X.shape)
        logging.info(y.shape)
    return X, y

def get_pickle_data(data_dir, verbose=True):
    pkl_file_x = open(data_dir + 'x.pkl', 'rb')
    x = pickle.load(pkl_file_x)
    pkl_file_x.close()
    if verbose:
        logging.info("X training loaded.")
        logging.info(x.shape)

    pkl_file_y = open(data_dir + 'y.pkl', 'rb')
    y = pickle.load(pkl_file_y)
    pkl_file_y.close()
    if verbose:
        logging.info("Y training loaded.")
        logging.info(y.shape)
    return x, y

def store(x, y, clip=True):
    if clip:
        x = x[:10000]
        y = y[:10000]
    output_x = open('x_clip.pkl', 'wb')
    pickle.dump(x, output_x)
    output_x.close()

    output_y = open('y_clip.pkl', 'wb')
    pickle.dump(y, output_y)
    output_y.close()

def collect_data(verbose=True):
    """
        Load the data for training.
        :param datapath: matlab data directory
        :param variable: variable of the matlab file
        :return: the data as numpy array
    """
    train_x = collect_trial_data(data_path=config['data_dir'], filename=config['cnn']['trainX_filename'],
                         variable1=config['cnn']['trainX_variable1'], variable2=config['cnn']['trainX_variable2'],
                         verbose=verbose, detailed_verbose=True)
    train_y = collect_trial_data(data_path=config['data_dir'], filename=config['cnn']['trainY_filename'],
                         variable1=config['cnn']['trainY_variable1'], variable2=config['cnn']['trainY_variable2'],
                         verbose=verbose, detailed_verbose=True)
    return train_x, train_y


def collect_trial_data(data_path, filename, variable1, variable2, verbose=True, detailed_verbose=False):
    """
    Extract data from the file.
    :param data_path: name of the file to open.
    :param verbose: boolean; if true, it logging.infos information about
    the status of the program.
    :return: a numpy array of shape ...?0
    """

    if verbose: logging.info("Loading data... ")
    if verbose: logging.info("Extracting trials...")
    trials = extract_trials()
    if verbose: logging.info(len(trials), " trials found.")

    full_data = np.array([])
    for i in range(20):
        if detailed_verbose: logging.info("Trying trial", trials[i])
        try:
            next_trial = load_matlab_trial(datapath=data_path, trial=trials[i], filename=filename, variable1=variable1,
                                       variable2=variable2)
            if full_data.size == 0:
                full_data = next_trial
            else:
                full_data = np.concatenate((full_data, next_trial))
            if detailed_verbose: logging.info(np.shape(full_data))
        except:
            logging.info("Trying other trials...")

    if verbose: logging.info("Data loaded.")
    return full_data


def load_matlab_trial(datapath, trial, filename, variable1, variable2):
    """
    Load the data from Matlab
    :param datapath: matlab data directory
    :param filename: name of the file from which the data must be loaded
    :param variable: variable of the matlab file
    :return: the data as numpy array
    """
    try:
        data = sio.loadmat(datapath + trial + "/" + filename)[variable1][variable2][0][0]
    except:
        logging.info("Trial " + trial + " could not be opened. ")
        raise Exception

    if len(np.shape(data)) == 3:
        data = np.swapaxes(data, 0, 2)
        data = np.swapaxes(data, 1, 2)
    else:
        data = data - 1  # y needs to be 0 or 1 (instead of 1 and 2)
    return data


def extract_trials():
    """
        Extracts the trials from the root directory
    """
    try:
        my_list = os.listdir(config['data_dir'])
    except:
        logging.info("Server unreachable. Cannot list the directories. Did you (maybe) forget to connect to the server by VPN? Is your root directory set correctly in config.py file? :)")
        raise Exception
    trials = [name for name in my_list if len(name) == 3]
    return trials
